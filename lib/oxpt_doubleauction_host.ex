defmodule Oxpt.DoubleAuction.Host do
  use Cizen.Automaton
  defstruct [:room_id, :double_auction_id]

  use Cizen.Effects
  alias Cizen.{Event, Filter}
  # alias Oxpt.JoinGame
  alias Oxpt.Game
  alias Oxpt.Player.{Input, Output}
  alias Oxpt.DoubleAuction.Host.{Reducer, PushState}

  @root_dir Path.join(__ENV__.file, "../..") |> Path.expand()

  @initial_state %{
    loading: false,
    mode: "wait",
    participants: %{},
    buyerBids: [],
    sellerBids: [],
    deals: [],
    highestBid: nil,
    lowestBid: nil,
    exType: "simple",
    priceBase: 100,
    priceInc: 100,
    priceMax: 20,
    priceMin: 10,
    firstVisit: true,
    hist: [],
    dynamicText: %{
      variables: %{
        seller: "売り手",
        buyer: "買い手",
        budget: "予算",
        cost: "仕入れ値",
        goods: "財",
        unit: "",
        price: "価格",
        profit: "利益",
        selling_price: "売値",
        buying_price: "買値"
      },
      desc: [
        "説明",
        "これからある<goods>の取引を行います。\n参加者は<seller>と<buyer>に分かれます。\nあなたは、なるべく多くの<profit>が出るように取引してください。",
        "あなたは<buyer>です。<budget>は<money><unit>です。\n<buyer>は、なるべく低い<price>で<goods>を購入し、多くの<profit>を上げてください。\nまた、<budget>よりも高い<price>で<goods>を購入することはできません。",
        "あなたは<seller>です。<cost>は<money><unit>です。\n<seller>は、なるべく高い<price>で<goods>を販売し、多くの<profit>を上げてください。\nまた、<cost>よりも低い<price>で<goods>を販売することはできません。",
        "提案<price>は正の整数で入力しなければなりません。また、提案<price>は取引が成立するまで何度でも変更することができます。"
      ],
      your_buyer: "あなたは<buyer>です。\n<budget>である<money><unit>以下の<price>で購入することができます。",
      your_seller: "あなたは<seller>です。\n<cost>である<money><unit>以上の<price>で販売することができます。"
    },
  }

  @behaviour Oxpt.Game
  @impl Oxpt.Game
  def build_assets(out_dir, public_url) do
    opts = [into: IO.stream(:stdio, :line), cd: @root_dir]
    System.cmd("npm", ["install", "--save-dev", "parcel"], opts)
    System.cmd("npm", ["run", "build", "--out-dir", out_dir, "--public-url", public_url], opts)
  end

  @impl Oxpt.Game
  def watch_assets(out_dir, public_url) do
    opts = [into: IO.stream(:stdio, :line), cd: @root_dir]
    System.cmd("npm", ["install", "--save-dev", "parcel"], opts)
    Game.run_watcher(["node", "./node_modules/parcel-bundler/bin/cli.js", "watch", "src/index.jsx", "--out-dir", out_dir, "--public-url", public_url], opts)
  end

  @impl Oxpt.Game
  def player_socket(game_id, _game, guest_id) do
    %__MODULE__.HostSocket{game_id: game_id, guest_id: guest_id}
  end

  @impl Oxpt.Game
  def new(room_id, double_auction_id: double_auction_id) do
    %__MODULE__{room_id: room_id, double_auction_id: double_auction_id}
  end

  # def handle_received(data, %{"action" => action, "params" => params}) do
  #   new = case action do
  #     "start" -> Host.start(data)
  #     "stop" -> Host.stop(data)
  #     "change_mode" -> Host.change_mode(data, params)
  #     "match" -> Host.match(data)
  #     "fetch_contents" -> Host.fetch_contents(data)
  #     "update_setting" -> Host.update_setting(data, params)
  #     "update_text" -> Host.update_text(data, params)
  #     "visit" -> Host.visit(data)
  #   end
  #   wrap_result(data, new)
  # end

  @impl true
  def spawn(id, %__MODULE__{}) do
    perform id, %Subscribe{
      event_filter: Filter.any([
        Filter.new(fn %Event{body: %Input{game_id: ^id}} -> true end),
        Filter.new(fn %Event{body: %PushState{game_id: ^id}} -> true end)
      ])
    }

    {:loop, @initial_state}
  end

  @impl true
  def yield(id, {:loop, state}) do
    event = perform id, %Receive{
      event_filter: Filter.new(fn
        %Event{body: %PushState{}} -> true
      end)
    }

    next_state = case event.body do
      %PushState{type: type, guest_id: guest_id, payload: payload} ->
        diff = case type do
          "visit" -> Reducer.visit(state)
          "update_setting" -> Reducer.update_setting(state, payload)
          "update_text" -> Reducer.update_text(state, payload)
          "change_mode" -> Reducer.change_mode(state, payload)
          "match" -> Reducer.match(state)
          "start" -> Reducer.start(state)
          "stop" -> Reducer.stop(state)
          _ -> state
        end
        perform id, %Dispatch{
          body: %Output{
            game_id: id,
            guest_id: guest_id,
            event: "update",
            payload: diff
          }
        }
        Map.merge(state, diff)
    end
    {:loop, next_state}
  end
end
