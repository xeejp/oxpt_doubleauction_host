defmodule Oxpt.DoubleAuction.Host.PushState do
  defstruct [:game_id, :guest_id, :type, :payload]
end
