defmodule Oxpt.DoubleAuction.Host.HostSocket do
  use Cizen.Automaton
  defstruct [:game_id, :guest_id]

  use Cizen.Effects
  alias Cizen.{Filter, Event}
  alias Oxpt.Player.Input
  alias Oxpt.DoubleAuction.Host.PushState

  @impl true
  def spawn(id, %__MODULE__{} = socket) do
    perform id, %Subscribe{
      event_filter: Filter.new(fn %Event{body: %Input{
        game_id: ^socket.game_id,
        guest_id: ^socket.guest_id
      }} -> true end)
    }

    perform id, %Dispatch{
      body: %PushState{
        game_id: socket.game_id,
        guest_id: socket.guest_id,
      }
    }

    {:loop, socket}
  end

  @impl true
  def yield(id, {:loop, socket}) do
    event = perform id, %Receive{
      event_filter: Filter.new(fn
        %Event{body: %Input{}} -> true
      end)
    }

    case event.body do
      %Input{event: type, payload: payload} ->
        perform id, %Dispatch{
          body: %PushState{
            game_id: socket.game_id,
            guest_id: socket.guest_id,
            type: type,
            payload: payload
          }
        }
    end
    {:loop, socket}
  end
end
