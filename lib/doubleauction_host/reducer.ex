defmodule Oxpt.DoubleAuction.Host.Reducer do

  def start(_state) do
    %{started: true}
  end

  def stop(_state) do
    %{started: false}
  end

  def change_mode(_state, params) do
    %{mode: params}
  end

  def match(state) do
    participants =
      Enum.shuffle(state.participants)
      |> Enum.map_reduce(0, fn {id, _participant}, acc ->
        new_participant = if rem(acc, 2) == 1 do
          %{
            role: "buyer",
            money: case state.ex_type do
              "simple" -> acc * state.price_inc + state.price_base
              "real"   -> :rand.uniform(state.price_max - state.price_min + 1) +state. price_min - 1
            end,
            bidded: false,
            bid: nil,
            dealt: false,
            deal: nil
          }
        else
          %{
            role: "seller",
            money: case state.ex_type do
              "simple" -> acc * state.price_inc + state.price_base
              "real"   -> :rand.uniform(state.price_max - state.price_min + 1) + state.price_min - 1
            end,
            bidded: false,
            bid: nil,
            dealt: false,
            deal: nil
          }
        end
        {{id, new_participant}, acc + 1}
      end)
      |> elem(0)
      |> Enum.into(%{})

    %{
      participants: participants,
      buyerBids: [],
      sellerBids: [],
      deals: [],
      highestBid: nil,
      lowestBid: nil,
      hist: []
    }
  end

  def update_setting(_state, params) do
    %{
      exType: params["exType"],
      priceBase: params["priceBase"],
      priceInc: params["priceInc"],
      priceMax: params["priceMax"],
      priceMin: params["priceMin"]
    }
  end

  def update_text(_state, params) do
    %{dynamicText: params}
  end

  def visit(_state) do
    %{firstVisit: false }
  end
end
