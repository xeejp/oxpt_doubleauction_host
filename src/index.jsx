import React from 'react'
import { render } from 'react-dom'
import { createStore, applyMiddleware } from 'redux'
import { Provider } from 'react-redux'
import { createLogger } from 'redux-logger'
import createSagaMiddleware from 'redux-saga'
import { createAction, createReducer } from 'redux-act'
import reduceReducers from 'reduce-reducers'
import { fork } from 'redux-saga/effects'
import { MuiThemeProvider } from 'material-ui/styles'
import channel from 'oxpt'
import App from './App.js'
import saga from './saga'
import reducer from './reducer'

function startApp(component, reducer, saga, host = false) {
  const logger = createLogger({ collapsed: true, diff: true })
  const sagaMiddleware = createSagaMiddleware()

  let middlewares = [sagaMiddleware, logger]

  const patch = createAction('patch', diff => diff)
  const patcher = createReducer({
    [patch]: (state, diff) => ({ ...state, ...diff })
  })

  const store = createStore(
    reduceReducers(reducer, patcher),
    applyMiddleware(...middlewares)
  )

  // Saga
  function * openParticipantPageSaga() {
    // while (true) {
    //   const { payload: id } = yield take(`${openParticipantPage}`)
    //   yield call(_experiment.openParticipantPage.bind(_experiment), id)
    // }
  }

  function * hostSaga() {
    yield fork(saga)
    yield fork(openParticipantPageSaga)
  }
  sagaMiddleware.run(hostSaga)

  channel.join()

  channel.on('update', (state) => {
    store.dispatch(patch(state))
  })

  window.sendData = function sendData(event, payload) {
    channel.push('input', { event: event, payload: payload || null })
  }

  const root = document.createElement('div')
  document.body.appendChild(root)

  render(
    <Provider store={store}>
      <MuiThemeProvider>
        {React.createElement(component)}
      </MuiThemeProvider>
    </Provider>,
    root
  )
}

startApp(App, reducer, saga, true)
