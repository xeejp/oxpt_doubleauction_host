/* eslint-disable react/prop-types */
import React from 'react'
import { connect } from 'react-redux'

import BidsTable from './components/BidsTable'
import Chart from './components/Chart'

const mapStateToProps = ({ buyerBids, sellerBids, deals, users, exType, priceBase, priceInc, priceMax, priceMin, dynamicText }) => ({
  buyerBids,
  sellerBids,
  deals,
  users,
  exType,
  priceBase,
  priceInc,
  priceMax,
  priceMin,
  dynamicText
})

const ScreenPage = ({ buyerBids, sellerBids, deals, users, exType, priceBase, priceInc, priceMax, priceMin, dynamicText }) => (
  <div>
    <BidsTable
      buyerBids={buyerBids}
      sellerBids={sellerBids}
      deals={deals}
      dynamicText={dynamicText}
    />
    <Chart
      users={users}
      deals={deals}
      expanded={true}
      ex_data={{ exType, priceBase, priceInc, priceMax, priceMin }}
      dynamicText={dynamicText}
    />
  </div>
)

export default connect(mapStateToProps)(ScreenPage)
