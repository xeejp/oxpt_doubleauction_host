/* eslint-disable react/prop-types */
import React, { Component } from 'react'
import { connect } from 'react-redux'
import keydown, { Keys } from 'react-keydown'
// import download from 'datauri-download'

import RaisedButton from 'material-ui/RaisedButton'
import Dialog from 'material-ui/Dialog'
import Divider from 'material-ui/Divider'
import TextField from 'material-ui/TextField'
import { Tabs, Tab } from 'material-ui/Tabs'

import SwipeableViews from 'react-swipeable-views'

import HostPage from './components/HostPage'
import Chart from './components/Chart'
import BidsTable from './components/BidsTable'
import Users from './Users'
import ScreenPage from './ScreenPage'

import { getPage, getExperimentType } from './util/index'
import { submitMode, updateSetting, updateText, visit } from './actions'

import { ReadJSON } from './util/ReadJSON'

const pages = ['wait', 'description', 'auction', 'result']
const exTypes = ['simple', 'real']

const mapStateToProps = ({ mode, loading, buyerBids, sellerBids, deals, highestBid, lowestBid, users, screenPage, exType, priceBase, priceInc, priceMax, priceMin, dynamicText, firstVisit, hist }) => ({
  mode,
  loading,
  buyerBids,
  sellerBids,
  highestBid,
  lowestBid,
  deals,
  users,
  screenPage,
  exType,
  priceBase,
  priceInc,
  priceMax,
  priceMin,
  dynamicText,
  firstVisit,
  hist,
})

const { ESC } = Keys

class App extends Component {
  constructor(props, context) {
    super(props, context)
    var dynamicText = this.props.dynamicText ? this.props.dynamicText : null
    this.state = {
      screenPage: false,
      setting: false,
      edit: false,
      disabled: false,
      slideIndex: 0,
      dynamicText: dynamicText,
    }
    this.handleOpenSetting = () => this.setState({
      setting: true,
      exType: this.props.exType,
      priceBase: this.props.priceBase,
      priceInc: this.props.priceInc,
      priceMax: this.props.priceMax,
      priceMin: this.props.priceMin,
    })
    this.handleOpenEdit = () => this.setState({
      edit: true,
      dynamicText: this.props.dynamicText,
    })
    this.handleSlide = value => this.setState({
      slideIndex: value
    })
    this.handleCloseScreenPage = () => this.setState({
      screenPage: false
    })
    this.handleChangePage = this.handleChangePage.bind(this)
    this.handleDownload = this.handleDownload.bind(this)
  }

  setting() {
    const { exType, priceBase, priceInc, priceMax, priceMin } = this.state

    var buttons = exTypes.map(type => <RaisedButton label={getExperimentType(type)} primary={exType === type} onClick={this.handleExChange.bind(this, type)} />)
    return (
      <span>
        {buttons} <br/>
        {(exType === 'simple')
          ? <span><TextField
            hintText={100}
            value={priceBase}
            floatingLabelText={ReadJSON().static_text['priceBase']}
            onChange={this.handleChangeText.bind(this, 'priceBase')}
          /><br/>
          <TextField
            hintText={100}
            value={priceInc}
            floatingLabelText={ReadJSON().static_text['priceInc']}
            onChange={this.handleChangeText.bind(this, 'priceInc')}
          /></span>
          : <span><TextField
            hintText={10}
            value={priceMin}
            floatingLabelText={ReadJSON().static_text['priceMin']}
            onChange={this.handleChangeText.bind(this, 'priceMin')}
          /><span style={{ marginLeft: '5px', marginRight: '5px' }}>～</span>
          <TextField
            hintText={20}
            value={priceMax}
            floatingLabelText={ReadJSON().static_text['priceMax']}
            onChange={this.handleChangeText.bind(this, 'priceMax')}
          /></span>
        }
      </span>
    )
  }

  handleChangeText(key, event) {
    const value = (event.target.value.length > 0) ? parseInt(event.target.value) : 0
    switch (key) {
      case 'priceBase': this.setState({ priceBase: value }); break
      case 'priceInc' : this.setState({ priceInc: value }); break
      case 'priceMax' : this.setState({ priceMax: value }); break
      case 'priceMin' : this.setState({ priceMin: value }); break
    }
    if (key === 'priceMax') this.setState({ disabled: value < this.state.priceMin })
    if (key === 'priceMin') this.setState({ disabled: this.state.priceMax < value })
  }

  handleExChange(value) {
    if (this.state.exType !== value) {
      this.setState({
        exType: value,
        priceBase: this.props.priceBase,
        priceInc: this.props.priceInc,
        priceMax: this.props.priceMax,
        priceMin: this.props.priceMin,
        disabled: false,
      })
    }
  }

  handleCloseSetting() {
    const { dispatch } = this.props
    const { exType, priceBase, priceInc, priceMax, priceMin } = this.state
    dispatch(updateSetting({ exType: exType, priceBase: priceBase, priceInc: priceInc, priceMax: priceMax, priceMin: priceMin }))
    this.setState({ setting: false })
  }

  questionEditer() {
    var tabs = [ReadJSON().static_text['pages'][1], ReadJSON().static_text['pages'][2], ReadJSON().static_text['var']].map((s, i) => <Tab label={s} value={i} />)
    var desc = (<span>
      <TextField
        hintText={ReadJSON().dynamicText['desc'][1]}
        defaultValue={this.props.dynamicText['desc'][1]}
        onBlur={this.handleChangeDynamicText.bind(this, ['desc', 1])}
        multiLine={true}
        fullWidth={true}
      />
      <div style={{ float: 'Left', width: '49%' }}><TextField
        hintText={ReadJSON().dynamicText['desc'][2]}
        defaultValue={this.props.dynamicText['desc'][2]}
        onBlur={this.handleChangeDynamicText.bind(this, ['desc', 2])}
        multiLine={true}
        fullWidth={true}
        rows={6}
        rowsMax={6}
      /></div>
      <div style={{ float: 'Right', width: '49%' }}><TextField
        hintText={ReadJSON().dynamicText['desc'][3]}
        defaultValue={this.props.dynamicText['desc'][3]}
        onBlur={this.handleChangeDynamicText.bind(this, ['desc', 3])}
        multiLine={true}
        fullWidth={true}
        rows={6}
        rowsMax={6}
      /></div>
      <div style={{ clear: 'both' }}><TextField
        hintText={ReadJSON().dynamicText['desc'][4]}
        defaultValue={this.props.dynamicText['desc'][4]}
        onBlur={this.handleChangeDynamicText.bind(this, ['desc', 4])}
        multiLine={true}
        fullWidth={true}
      /></div>
    </span>)
    var ex = (<span>
      <div style={{ float: 'Left', width: '49%' }}><TextField
        hintText={ReadJSON().dynamicText['your_buyer']}
        defaultValue={this.props.dynamicText['your_buyer']}
        onBlur={this.handleChangeDynamicText.bind(this, ['your_buyer'])}
        multiLine={true}
        fullWidth={true}
        rows={7}
        rowsMax={7}
      /></div>
      <div style={{ float: 'Right', width: '49%' }}><TextField
        hintText={ReadJSON().dynamicText['your_seller']}
        defaultValue={this.props.dynamicText['your_seller']}
        onBlur={this.handleChangeDynamicText.bind(this, ['your_seller'])}
        multiLine={true}
        fullWidth={true}
        rows={7}
        rowsMax={7}
      /></div>
    </span>)

    var variables = (<div>
      {['seller', 'buyer', 'budget', 'cost', 'goods', 'unit', 'price', 'profit', 'selling_price', 'buying_price'].map((key, i) =>
        <div style={{ width: '49%', float: (i % 2 === 0) ? 'Left' : 'Right' }}>
          <TextField
            hintText={key}
            defaultValue={this.props.dynamicText['variables'][key]}
            onBlur={this.handleChangeDynamicText.bind(this, ['variables', key])}
          />
        </div>
      )}
    </div>)

    return (<span>
      <Tabs
        onChange={this.handleSlide}
        value={this.state.slideIndex}
      >
        {tabs}
      </Tabs>
      <SwipeableViews
        index={this.state.slideIndex}
        onChangeIndex={this.handleSlide}
      >
        {desc}
        {ex}
        {variables}
      </SwipeableViews>
    </span>)
  }

  handleChangeDynamicText(value, event) {
    var dynamicText = Object.assign({}, this.state.dynamicText)
    var temp = dynamicText
    for (var i = 0; i < value.length - 1; i++) {
      temp = temp[value[i]]
    }
    temp[value[value.length - 1]] = event.target.value
    this.setState({ dynamicText: dynamicText })
  }

  handleCloseEdit() {
    const { dispatch } = this.props
    dispatch(updateText(this.state.dynamicText))
    this.setState({
      edit: false,
      slideIndex: 0,
    })
  }

  handleChangePage(page) {
    this.props.dispatch(submitMode(page))
  }

  handleDownload() {
    const { users, hist } = this.props
    const fileName = ReadJSON().static_text['file'][0]
    const list = [
      [ReadJSON().static_text['file'][1]],
      [ReadJSON().static_text['file'][2], new Date()],
      [ReadJSON().static_text['file'][3], Object.keys(users).length],
      [ReadJSON().static_text['file'][4], ReadJSON().static_text['file'][5], ReadJSON().static_text['file'][6], ReadJSON().static_text['file'][7], ReadJSON().static_text['file'][8]],
      ...(Object.keys(users).map(id => {
        const user = users[id]
        return [id, user.role, user.money, user.bid, user.deal]
      })),
      [],
      [ReadJSON().static_text['file'][9]],
      [ReadJSON().static_text['file'][10], ReadJSON().static_text['file'][11], ReadJSON().static_text['file'][12], ReadJSON().static_text['file'][13], ReadJSON().static_text['file'][14]],
      ...(hist.map(history => {
        return [
          history.status,
          history.price,
          history.id1,
          history.id2 ? history.id2 : '-',
          history.time,
        ]
      }))
    ]
    const content = list.map(line => line.join(',')).join('\n')
    let bom = new Uint8Array([0xEF, 0xBB, 0xBF])
    let blob = new Blob([bom, content])
    let url = window.URL || window.webkitURL
    let blobURL = url.createObjectURL(blob)

    let a = document.createElement('a')
    a.download = fileName
    a.href = blobURL
    a.click()
    //   download(fileName, 'text/csv;charset=utf-8', content)
  }

  componentWillReceiveProps({ dispatch, keydown, mode: nextPage, firstVisit }) {
    if (firstVisit) {
      this.handleOpenSetting()
      dispatch(visit())
    }

    if (keydown.event && keydown.event.which === ESC) {
      this.setState({
        screenPage: false
      })
    }
    if (this.props.mode !== nextPage) {
      if (nextPage === 'result') {
        // TODO: Add Materialize
        // Materialize.toast(ReadJSON().static_text['push_esc'], 5000, 'rounded')
        this.setState({
          screenPage: true
        })
      }
    }
  }

  render() {
    const { mode, loading, buyerBids, sellerBids, deals, highestBid, lowestBid, users, exType, priceBase, priceInc, priceMax, priceMin, dynamicText } = this.props

    if (this.state.screenPage) {
      return (
        <div>
          <ScreenPage />
          <RaisedButton
            label={ReadJSON().static_text['back_top']}
            onClick={this.handleCloseScreenPage}
            style={{
              marginTop: '5%',
            }}
          />
        </div>
      )
    } else {
      return (
        <HostPage
          title={'HOST'}
          page={mode}
          getPageName={getPage}
          pages={pages}
          changePage={this.handleChangePage}
          openSettingDialog={this.handleOpenSetting}
          openEditDialog={this.handleOpenEdit}
          downloadFile={this.handleDownload}
          loading={loading}
          settingButton={mode === 'wait'}
          editButton={mode === 'wait'}
          downloadButton={mode === 'result'}
          text={ReadJSON().static_text}
        >
          <div style={{ marginTop: '5%' }}>
            <Users />
          </div>
          <Divider
            style={{
              marginTop: '5%',
            }}
          />
          <BidsTable
            buyerBids={buyerBids}
            sellerBids={sellerBids}
            deals={deals}
            highestBid={highestBid}
            lowestBid={lowestBid}
            expanded={false}
            dynamicText={dynamicText}
          />
          <Divider
            style={{
              marginTop: '5%',
            }}
          />
          <Chart
            users={users}
            deals={deals}
            expanded={false}
            ex_data={{ exType, priceBase, priceInc, priceMax, priceMin }}
            dynamicText={dynamicText}
          />
          <Dialog
            title={ReadJSON().static_text['option']}
            actions={[
              <RaisedButton
                label={ReadJSON().static_text['apply']}
                primary={true}
                onClick={this.handleCloseSetting.bind(this)}
                disabled={this.state.disabled}
              />
            ]}
            model={false}
            open={this.state.setting}
            autoScrollBodyContent={true}
          >
            {this.setting()}
          </Dialog>
          <Dialog
            title={ReadJSON().static_text['edit']}
            actions={[
              <RaisedButton
                label={ReadJSON().static_text['apply']}
                primary={true}
                onClick={this.handleCloseEdit.bind(this)}
              />
            ]}
            model={false}
            open={this.state.edit}
            autoScrollBodyContent={true}
          >
            {loading ? null : this.questionEditer()}
          </Dialog>
        </HostPage>
      )
    }
  }
}

export default connect(mapStateToProps)(keydown(App))
