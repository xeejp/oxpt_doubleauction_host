import { enableScreenMode, disableScreenMode } from './actions'
import { createReducer } from 'redux-act'

const reducer = createReducer({
  [enableScreenMode]: (state, action) => Object.assign({}, state, { screenMode: true }),
  [disableScreenMode]: (state, action) => Object.assign({}, state, { screenMode: false })
}, {
  loading: true,
  screenMode: false,
})
export default reducer
